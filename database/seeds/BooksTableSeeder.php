<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                    'title' => 'Game Of Thrones 1',
                    'author' => 'George R. R. Martin',
                    'user_id' => '1',
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'title' => 'Game Of Thrones 2',
                    'author' => 'George R. R. Martin',
                    'user_id' => '1',
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'title' => 'Harry Potter 1',
                    'author' => 'J.K.Rolling',
                    'user_id' => '2',
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'title' => 'Harry Potter 2',
                    'author' => 'J.K.Rolling',
                    'user_id' => '2',
                    'created_at' => date('Y,m,d G:i:s'),
                ],
                [
                    'title' => 'Game Of Thrones 3',
                    'author' => 'George R. R. Martin',
                    'user_id' => '2',
                    'created_at' => date('Y,m,d G:i:s'),
                ],

            ]

            );
    }
}
