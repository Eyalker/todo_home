<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'Eyal',
                'email'=>'Eyal@gmail.com',
                'password' => '12345678',
                'created_at' => date('y-m-d G:i:s'),
                ],
                [
                'name'=> 'jack2',
                'email'=>'jack11@jack.com',
                'password' => '12345678',
                'created_at' => date('y-m-d G:i:s'),
                ],
            ]
    );
    }
}
