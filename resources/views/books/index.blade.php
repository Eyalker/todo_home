<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    </head>
    <body>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author</th>
                    <th scope="col">User ID</th>
                </tr>
                @foreach ($books as $book)
            </thead>
            <tbody>
                <tr>
                    <td scope="row"> {{$book->id}} </td>
                    <td scope="row"> {{$book->title}} </td>
                    <td scope="row"> {{$book->author}} </td>
                    <td scope="row"> {{$book->user_id}} </td>
                </tr> 
                @endforeach
            </tbody>
        </table>
    </body>
</html>